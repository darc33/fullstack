<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('paths','pathscontroller');
Route::resource('directions','directionscontroller');
Route::resource('routes','routescontroller');
Route::resource('schedules','schedulescontroller');
Route::resource('stops','stopscontroller');
Route::resource('routes_bics','routesbicscontroller');
Route::resource('routes_parts','routespartscontroller');
Route::resource('fav_routes','favroutescontroller');
Route::resource('fav_directions','favdirectionscontroller');
Route::resource('transmilenios','transmilenioscontroller');
Route::resource('sitps','sitpscontroller');
Route::resource('alimentadores','alimentadorescontroller');
