<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes_parts', function (Blueprint $table) {
            $table->increments('id_part');
            $table->string('part_name',100);
            $table->unsignedInteger('Path_id');
            $table->foreign('Path_id')->references('id_path')->on('paths');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes_parts');
    }
}
