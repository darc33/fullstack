<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesBicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes_bics', function (Blueprint $table) {
            $table->increments('id_route_bic');
            $table->string('bic_name',100);
            $table->unsignedInteger('Path_id');
            $table->foreign('Path_id')->references('id_path')->on('paths');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes_bics');
    }
}
