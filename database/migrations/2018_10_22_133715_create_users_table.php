<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id_user');
            $table->string('password',100);
            $table->string('name',100);
            $table->string('surname',100);
            $table->integer('telephone');
            $table->unsignedInteger('fav_route_id');
            $table->unsignedInteger('fav_direction_id');
            $table->unsignedInteger('route_bic_id');
            $table->unsignedInteger('part_id');
            $table->unsignedInteger('tm_id');
            $table->unsignedInteger('sitp_id');
            $table->unsignedInteger('alimentador_id');
            $table->foreign('fav_route_id')->references('id_fav_route')->on('fav_routes');
            $table->foreign('fav_direction_id')->references('id_fav_direction')->on('fav_directions');
            $table->foreign('route_bic_id')->references('id_route_bic')->on('routes_bics');
            $table->foreign('part_id')->references('id_part')->on('routes_parts');
            $table->foreign('tm_id')->references('id_tm')->on('transmilenios');
            $table->foreign('sitp_id')->references('id_sitp')->on('sitps');
            $table->foreign('alimentador_id')->references('id_alimentador')->on('alimentadores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
