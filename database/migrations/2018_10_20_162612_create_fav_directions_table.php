<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavDirectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fav_directions', function (Blueprint $table) {
            $table->increments('id_fav_direction');
            $table->string('fav_direction_name',100);
            $table->unsignedInteger('direction_id');
            $table->foreign('direction_id')->references('id_direction')->on('directions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fav_directions');
    }
}
