<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fav_routes', function (Blueprint $table) {
            $table->increments('id_fav_route');
            $table->string('fav_route_name',100);
            $table->unsignedInteger('Path_id');
            $table->foreign('Path_id')->references('id_path')->on('paths');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fav_routes');
    }
}
