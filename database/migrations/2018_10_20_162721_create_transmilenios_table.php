<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransmileniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transmilenios', function (Blueprint $table) {
            $table->increments('id_tm');
            $table->string('tm_name',100);
            $table->unsignedInteger('route_id');
            $table->unsignedInteger('stop_id');
            $table->unsignedInteger('schedule_id');
            $table->foreign('route_id')->references('id_route')->on('routes');
            $table->foreign('stop_id')->references('id_stop')->on('stops');
            $table->foreign('schedule_id')->references('id_schedule')->on('schedules');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transmilenios');
    }
}
