<?php

namespace App\Http\Controllers;

use App\directions;
use Illuminate\Http\Request;

class directionscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "directions";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\directions  $directions
     * @return \Illuminate\Http\Response
     */
    public function show(directions $directions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\directions  $directions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, directions $directions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\directions  $directions
     * @return \Illuminate\Http\Response
     */
    public function destroy(directions $directions)
    {
        //
    }
}
