<?php

namespace App\Http\Controllers;

use App\fav_routes;
use Illuminate\Http\Request;

class favroutescontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "fav_routes";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\fav_routes  $fav_routes
     * @return \Illuminate\Http\Response
     */
    public function show(fav_routes $fav_routes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\fav_routes  $fav_routes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fav_routes $fav_routes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\fav_routes  $fav_routes
     * @return \Illuminate\Http\Response
     */
    public function destroy(fav_routes $fav_routes)
    {
        //
    }
}
