<?php

namespace App\Http\Controllers;

use App\alimentadores;
use Illuminate\Http\Request;

class alimentadorescontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "alimentadores";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\alimentadores  $alimentadores
     * @return \Illuminate\Http\Response
     */
    public function show(alimentadores $alimentadores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\alimentadores  $alimentadores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, alimentadores $alimentadores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\alimentadores  $alimentadores
     * @return \Illuminate\Http\Response
     */
    public function destroy(alimentadores $alimentadores)
    {
        //
    }
}
