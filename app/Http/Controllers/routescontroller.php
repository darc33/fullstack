<?php

namespace App\Http\Controllers;

use App\routes;
use Illuminate\Http\Request;

class routescontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "routes";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\routes  $routes
     * @return \Illuminate\Http\Response
     */
    public function show(routes $routes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\routes  $routes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, routes $routes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\routes  $routes
     * @return \Illuminate\Http\Response
     */
    public function destroy(routes $routes)
    {
        //
    }
}
