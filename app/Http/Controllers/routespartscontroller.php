<?php

namespace App\Http\Controllers;

use App\routes_parts;
use Illuminate\Http\Request;

class routespartscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "routes_parts";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\routes_parts  $routes_parts
     * @return \Illuminate\Http\Response
     */
    public function show(routes_parts $routes_parts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\routes_parts  $routes_parts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, routes_parts $routes_parts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\routes_parts  $routes_parts
     * @return \Illuminate\Http\Response
     */
    public function destroy(routes_parts $routes_parts)
    {
        //
    }
}
