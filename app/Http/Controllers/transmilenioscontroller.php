<?php

namespace App\Http\Controllers;

use App\transmilenios;
use Illuminate\Http\Request;

class transmilenioscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "transmilenios";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\transmilenios  $transmilenios
     * @return \Illuminate\Http\Response
     */
    public function show(transmilenios $transmilenios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\transmilenios  $transmilenios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, transmilenios $transmilenios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\transmilenios  $transmilenios
     * @return \Illuminate\Http\Response
     */
    public function destroy(transmilenios $transmilenios)
    {
        //
    }
}
