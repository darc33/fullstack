<?php

namespace App\Http\Controllers;

use App\stops;
use Illuminate\Http\Request;

class stopscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "stops";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\stops  $stops
     * @return \Illuminate\Http\Response
     */
    public function show(stops $stops)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\stops  $stops
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, stops $stops)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\stops  $stops
     * @return \Illuminate\Http\Response
     */
    public function destroy(stops $stops)
    {
        //
    }
}
