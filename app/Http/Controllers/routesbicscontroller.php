<?php

namespace App\Http\Controllers;

use App\routes_bics;
use Illuminate\Http\Request;

class routesbicscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "routes_bics";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\routes_bics  $routes_bics
     * @return \Illuminate\Http\Response
     */
    public function show(routes_bics $routes_bics)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\routes_bics  $routes_bics
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, routes_bics $routes_bics)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\routes_bics  $routes_bics
     * @return \Illuminate\Http\Response
     */
    public function destroy(routes_bics $routes_bics)
    {
        //
    }
}
