<?php

namespace App\Http\Controllers;

use App\paths;
use Illuminate\Http\Request;

class pathscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "paths";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\paths  $paths
     * @return \Illuminate\Http\Response
     */
    public function show(paths $paths)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\paths  $paths
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, paths $paths)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\paths  $paths
     * @return \Illuminate\Http\Response
     */
    public function destroy(paths $paths)
    {
        //
    }
}
