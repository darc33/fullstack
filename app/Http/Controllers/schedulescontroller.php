<?php

namespace App\Http\Controllers;

use App\schedules;
use Illuminate\Http\Request;

class schedulescontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return schedules::all();
        // $data=schedules::all();
        // return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //con post: http://localhost:8000/api/schedules
        //$rules =['day'=>'required];
        //$rules =array(
        //    'day'=>'required'
        //    'email'=>'required|email'
        //);
        //$this->validate($request,$rules);  
        $schedule = schedules::create($request->all());
        return $schedule;
        //return response()-> json($schedule, 201);//http object created
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\schedules  $schedules
     * @return \Illuminate\Http\Response
     */
    public function show(schedules $schedules)
    {
        //con get: http://localhost:8000/api/schedules/2
        return $schedules;
        //$schedules = schedules::find($schedules);
        //return($schedules);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\schedules  $schedules
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, schedules $schedules)
    {
        //Con put o patch
        //$schedule=$schedules::find($id)
        // $schedules->fill($request->all());
        // $schedules->save();
        $schedules->update($request->all());
        return $schedules;
        //return response()-> json($schedule, 200);//http ok
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\schedules  $schedules
     * @return \Illuminate\Http\Response
     */
    public function destroy(schedules $schedules)
    {
        $schedules->delete();
        return 'ok';
        //return response()-> json(null, 204);//http no content
    }
}
