<?php

namespace App\Http\Controllers;

use App\fav_directions;
use Illuminate\Http\Request;

class favdirectionscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "fav_directions";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\fav_directions  $fav_directions
     * @return \Illuminate\Http\Response
     */
    public function show(fav_directions $fav_directions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\fav_directions  $fav_directions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fav_directions $fav_directions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\fav_directions  $fav_directions
     * @return \Illuminate\Http\Response
     */
    public function destroy(fav_directions $fav_directions)
    {
        //
    }
}
