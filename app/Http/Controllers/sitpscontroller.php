<?php

namespace App\Http\Controllers;

use App\sitps;
use Illuminate\Http\Request;

class sitpscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "sitps";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sitps  $sitps
     * @return \Illuminate\Http\Response
     */
    public function show(sitps $sitps)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sitps  $sitps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sitps $sitps)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sitps  $sitps
     * @return \Illuminate\Http\Response
     */
    public function destroy(sitps $sitps)
    {
        //
    }
}
